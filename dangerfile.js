import { message, danger } from 'danger'
import yarn from 'danger-plugin-yarn'

const modifiedFiles = danger.git.modified_files.join('\n - ')

message(`Changed files in this MR: \n - ${ modifiedFiles }`)

yarn()